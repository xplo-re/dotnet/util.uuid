﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

namespace XploRe.Util;

/// <summary>
///     Supported UUID versions. The numeric value corresponds to the version number.
/// </summary>
public enum UuidVersion
{

    /// <summary>
    ///     Unknown UUID version (version bits not set).
    /// </summary>
    Unknown = default,

    /// <summary>
    ///     Time and MAC-based version 1 UUID. Designates 48 bits to the MAC address of the node, 60 bits to the
    ///     timestamp equal to the number of 100-nanosecond intervals since midnight 15 October 1582 UTC, 13 or 14
    ///     bites for a clock sequence that extends the timestamp in case of interleaved requests.
    ///     In case no MAC address is available or should not be exposed, the node ID can be replaced by a random
    ///     48-bit node ID, but requires the corresponding MAC multicast bit to be set to 1 (LSB in first octet).
    ///     UUID generation is limited to a maximum average of 163 billion unique UUIDs per node per second.
    /// </summary>
    TimeBased = 1,

    /// <summary>
    ///     Time and MAC-based version 2 UUID for DCE security. Truncates the clock sequence by 8 bits to include a
    ///     local domain number, and truncates the timestamp by 32 bits to include a local-domain specific integer
    ///     identifier. Due to the reduced clock sequence and timestamp fields, UUID generation is limited to about
    ///     once unique UUID per node/domain/identifier per 7 seconds.
    /// </summary>
    TimeBasedDceSecurity = 2,

    /// <summary>
    ///     Name-based version 3 UUID created by computing a MD5-hash of a namespace identifier UUID and a given
    ///     name. The same namespace identifier and name will always yield the same UUID. In total, 121 or 122 bits
    ///     contribute to the uniqueness of a version 3 UUID. RFC 4122 recommends version 5 UUIDs over version 3
    ///     UUIDs.
    /// </summary>
    NameBasedMd5 = 3,

    /// <summary>
    ///     Randomly generated UUID. In total, 121 or 122 bits contribute to the uniqueness of a version 4 UUID.
    /// </summary>
    Random = 4,

    /// <summary>
    ///     Name-based version 5 UUID created by computing a SHA-1-hash of a namespace identifier UUID and a given
    ///     name. The same namespace identifier and name will always yield the same UUID. In total, 121 or 122 bits
    ///     contribute to the uniqueness of a version 5 UUID.
    /// </summary>
    NameBasedSha1 = 5,

}
