﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Runtime.InteropServices;


namespace XploRe.Util;

/// <summary>
///     Extension methods to convert between <see cref="Guid" /> and <see cref="Uuid" /> instances.
/// </summary>
public static class GuidExtensions
{

    /// <summary>
    ///     Returns a new <see cref="Uuid" /> instance that represents the same GUID.
    /// </summary>
    /// <param name="guid">This <see cref="Guid" /> instance to convert.</param>
    /// <returns>New <see cref="Uuid" /> instance that represents the same GUID.</returns>
    public static Uuid ToUuid(this Guid guid)
    {
        var handle = GCHandle.Alloc(guid, GCHandleType.Pinned);
        var uuid = Marshal.PtrToStructure<Uuid>(handle.AddrOfPinnedObject());
        handle.Free();

        return uuid;
    }

}
