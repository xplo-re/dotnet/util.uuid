﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using FluentAssertions;
using JetBrains.Annotations;
using XploRe.Runtime;
using Xunit;


// ReSharper disable EqualExpressionComparison
// ReSharper disable PossibleNullReferenceException

namespace XploRe.Util.Tests
{

    public class UuidTest
    {

        // Specific version UUIDs.

        private const string UuidVersion1 = "255ab20a-ca4c-11e7-abc4-cec278b6b50a";
        private const string UuidVersion3 = "6fa459ea-ee8a-3ca4-894e-db77e160355e";
        private const string UuidVersion4 = "988629e4-9397-4208-9a69-ea9d21b4ebd1";
        private const string UuidVersion5 = "886313e1-3b8a-5372-9b90-0c9aee199e5d";
        private const string UuidMySql = "08d52ae4-254d-97fe-f524-2b4833e26be6";

        // Hexadecimal UUID for better visualisation.

        private const string UuidHex = "00010203-0405-0607-0809-0a0b0c0d0e0f";

        private static readonly byte[] UuidHexBigEndianBytes = {
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
        };

        private static readonly byte[] UuidHexLittleEndianBytes = {
            0x03, 0x02, 0x01, 0x00, 0x05, 0x04, 0x07, 0x06, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
        };

        private static class Generator
        {

            /// <summary>
            ///     Yields a UUID string, the corresponding version and the variant. Version and variant are explicitly
            ///     provided as numbers for compatibility with other environments such as Java. Use of the corresponding
            ///     enumerations is necessary in the test methods, as otherwise comparisons may fail.
            /// </summary>
            public static IEnumerable<object[]> UuidProperties
            {
                [UsedImplicitly]
                get {
                    yield return new object[] { UuidVersion1, 1, 2 };
                    yield return new object[] { UuidVersion3, 3, 2 };
                    yield return new object[] { UuidVersion4, 4, 2 };
                    yield return new object[] { UuidVersion5, 5, 2 };
                    /*
                    yield return new object[] { UuidMySql, 9, 7 };
                    */
                    yield return new object[] { UuidHex, 0, 0 };
                }
            }

            public static IEnumerable<object[]> UuidSortOrders
            {
                [UsedImplicitly]
                get {
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "01010203-0405-0607-0809-0a0b0c0d0e0f",
                        "02010203-0405-0607-0809-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00020203-0405-0607-0809-0a0b0c0d0e0f",
                        "00030203-0405-0607-0809-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010303-0405-0607-0809-0a0b0c0d0e0f",
                        "00010403-0405-0607-0809-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010204-0405-0607-0809-0a0b0c0d0e0f",
                        "00010205-0405-0607-0809-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0505-0607-0809-0a0b0c0d0e0f",
                        "00010203-0605-0607-0809-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0406-0607-0809-0a0b0c0d0e0f",
                        "00010203-0407-0607-0809-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0707-0809-0a0b0c0d0e0f",
                        "00010203-0405-0807-0809-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0608-0809-0a0b0c0d0e0f",
                        "00010203-0405-0609-0809-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0607-0909-0a0b0c0d0e0f",
                        "00010203-0405-0607-0a09-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0607-080a-0a0b0c0d0e0f",
                        "00010203-0405-0607-080b-0a0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0607-0809-0b0b0c0d0e0f",
                        "00010203-0405-0607-0809-0c0b0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0607-0809-0a0c0c0d0e0f",
                        "00010203-0405-0607-0809-0a0d0c0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0607-0809-0a0b0d0d0e0f",
                        "00010203-0405-0607-0809-0a0b0e0d0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0607-0809-0a0b0c0e0e0f",
                        "00010203-0405-0607-0809-0a0b0c0f0e0f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0607-0809-0a0b0c0d0f0f",
                        "00010203-0405-0607-0809-0a0b0c0d100f"
                    };
                    yield return new object[] {
                        "00010203-0405-0607-0809-0a0b0c0d0e0f",
                        "00010203-0405-0607-0809-0a0b0c0d0e10",
                        "00010203-0405-0607-0809-0a0b0c0d0e11"
                    };
                }
            }

        }

        [Fact]
        public void UuidString()
        {
            var uuid = new Uuid(UuidVersion1);

            uuid.ToString().Should().Be(UuidVersion1);
        }

        [Fact]
        public void UuidGuidBridge()
        {
            var uuid = (Guid) new Uuid(UuidVersion1);
            var guid = new Guid(UuidVersion1);

            uuid.ShouldBeEquivalentTo(guid);
        }

        [Fact]
        public void GuidUuidBridge()
        {
            var uuid = new Uuid(UuidVersion1);
            var guid = (Uuid) new Guid(UuidVersion1);

            guid.ShouldBeEquivalentTo(uuid);
        }

#pragma warning disable xUnit1026

        [Theory]
        [MemberData(nameof(Generator.UuidProperties), MemberType = typeof(Generator))]
        public void UuidGuidHashCodeCompatibility([NotNull] string uuidString, UuidVersion version, UuidVariant variant)
        {
            var uuid = new Uuid(uuidString);
            var guid = new Guid(uuidString);

            uuid.GetHashCode().Should().Be(guid.GetHashCode());
        }

        [Theory]
        [MemberData(nameof(Generator.UuidProperties), MemberType = typeof(Generator))]
        public void UuidGuidStringCompatibility([NotNull] string uuidString, UuidVersion version, UuidVariant variant)
        {
            var uuid = new Uuid(uuidString);
            var guid = new Guid(uuidString);

            uuid.ToString().Should().Be(guid.ToString());
        }

        [Theory]
        [MemberData(nameof(Generator.UuidProperties), MemberType = typeof(Generator))]
        public void UuidGuidByteArrayCompatibility(
            [NotNull] string uuidString,
            UuidVersion version, UuidVariant variant)
        {
            var uuid = new Uuid(uuidString);
            var guid = new Guid(uuidString);

            uuid.ToByteArray().Should().BeEquivalentTo(guid.ToByteArray());
        }

#pragma warning restore xUnit1026

        [Theory]
        [MemberData(nameof(Generator.UuidProperties), MemberType = typeof(Generator))]
        public void UuidProperties([NotNull] string uuidString, UuidVersion version, UuidVariant variant)
        {
            var uuid = new Uuid(uuidString);

            uuid.Version.Should().Be(version);
            uuid.Variant.Should().Be(variant);
        }

        [Theory]
        [MemberData(nameof(Generator.UuidProperties), MemberType = typeof(Generator))]
        public void UuidStringOperator([NotNull] string uuidString, UuidVersion version, UuidVariant variant)
        {
            var uuid = (Uuid) uuidString;

            uuid.Version.Should().Be(version);
            uuid.Variant.Should().Be(variant);
            uuid.ToString().Should().Be(uuidString);
        }

        [Fact]
        public void ByteArrayBigEndian()
        {
            var uuid = new Uuid(UuidHex);

            uuid.ToByteArray(ByteOrder.BigEndian).Should().BeEquivalentTo(UuidHexBigEndianBytes);
        }

        [Fact]
        public void ByteArrayLittleEndian()
        {
            var uuid = new Uuid(UuidHex);

            uuid.ToByteArray(ByteOrder.LittleEndian).Should().BeEquivalentTo(UuidHexLittleEndianBytes);
        }

        [Fact]
        public void ConstructFromByteArrayHostOrder()
        {
            Uuid uuid;

            if (BitConverter.IsLittleEndian) {
                uuid = new Uuid(UuidHexLittleEndianBytes);
            }
            else {
                uuid = new Uuid(UuidHexBigEndianBytes);
            }

            uuid.ToString().Should().Be(UuidHex);
        }

        [Fact]
        public void ConstructFromByteArrayLittleEndian()
        {
            var uuid = new Uuid(UuidHexLittleEndianBytes, ByteOrder.LittleEndian);

            uuid.ToString().Should().Be(UuidHex);
        }

        [Fact]
        public void ConstructFromByteArrayBigEndian()
        {
            var uuid = new Uuid(UuidHexBigEndianBytes, ByteOrder.BigEndian);

            uuid.ToString().Should().Be(UuidHex);
        }

        [Fact]
        public void ByteArrayRoundTrip()
        {
            var uuid1 = new Uuid(UuidHex);
            var bytes = uuid1.ToByteArray();
            var uuid2 = new Uuid(bytes);

            uuid2.ShouldBeEquivalentTo(uuid1);
        }

        [Fact]
        public void ByteArrayRoundTripBigEndian()
        {
            var uuid1 = new Uuid(UuidHex);
            var bytes = uuid1.ToByteArray(ByteOrder.BigEndian);
            var uuid2 = new Uuid(bytes, ByteOrder.BigEndian);

            bytes.ShouldAllBeEquivalentTo(UuidHexBigEndianBytes);
            uuid2.ShouldBeEquivalentTo(uuid1);
        }

        [Fact]
        public void ByteArrayRoundTripLittleEndian()
        {
            var uuid1 = new Uuid(UuidHex);
            var bytes = uuid1.ToByteArray(ByteOrder.LittleEndian);
            var uuid2 = new Uuid(bytes, ByteOrder.LittleEndian);

            bytes.ShouldAllBeEquivalentTo(UuidHexLittleEndianBytes);
            uuid2.ShouldBeEquivalentTo(uuid1);
        }

        [Fact]
        public void Equality()
        {
            var uuid1 = new Uuid(UuidHex);
            var uuid2 = new Uuid(UuidHex);
            var guid1 = (Guid) uuid1;

            uuid1.Equals((object) uuid2).Should().BeTrue();
            uuid2.Equals(uuid1).Should().BeTrue();
            (uuid2 == uuid1).Should().BeTrue();
            (uuid1 == uuid2).Should().BeTrue();
            (uuid2 != uuid1).Should().BeFalse();
            (uuid1 != uuid2).Should().BeFalse();

            // Implicit conversion.
            uuid2.Equals(guid1).Should().BeTrue();
            (uuid1 == guid1).Should().BeTrue();
            (guid1 == uuid1).Should().BeTrue();
            (uuid1 != guid1).Should().BeFalse();
            (guid1 != uuid1).Should().BeFalse();
        }

        [Fact]
        public void Inequality()
        {
            var uuid1 = new Uuid(UuidHex);
            var uuid2 = new Uuid(UuidMySql);
            var guid1 = (Guid) uuid1;

            uuid1.Equals(null).Should().BeFalse();
            // ReSharper disable once SuspiciousTypeConversion.Global
            uuid1.Equals((object) (Guid) uuid1).Should().BeFalse();

            uuid1.Equals((object) uuid2).Should().BeFalse();
            uuid2.Equals(uuid1).Should().BeFalse();
            (uuid1 == uuid2).Should().BeFalse();
            (uuid2 == uuid1).Should().BeFalse();
            (guid1 == uuid2).Should().BeFalse();
            (uuid2 == guid1).Should().BeFalse();
            (uuid1 != uuid2).Should().BeTrue();
            (uuid2 != uuid1).Should().BeTrue();
            (guid1 != uuid2).Should().BeTrue();
            (uuid2 != guid1).Should().BeTrue();

            // Implicit conversion.
            uuid2.Equals(guid1).Should().BeFalse();
            (uuid2 == guid1).Should().BeFalse();
            (guid1 == uuid2).Should().BeFalse();
            (uuid2 != guid1).Should().BeTrue();
            (guid1 != uuid2).Should().BeTrue();
        }

        [Theory]
        [MemberData(nameof(Generator.UuidSortOrders), MemberType = typeof(Generator))]
        public void SortOrder(string a, string b, string c)
        {
            var uuid1 = new Uuid(a);
            var uuid2 = new Uuid(b);
            var uuid3 = new Uuid(c);

            uuid1.CompareTo(uuid1).Should().Be(0);
            uuid1.CompareTo(uuid2).Should().BeNegative();
            uuid2.CompareTo(uuid1).Should().BePositive();
            uuid2.CompareTo(uuid2).Should().Be(0);
            uuid2.CompareTo(uuid3).Should().BeNegative();
            uuid3.CompareTo(uuid2).Should().BePositive();
            uuid3.CompareTo(uuid3).Should().Be(0);
            uuid1.CompareTo(uuid3).Should().BeNegative();
            uuid3.CompareTo(uuid1).Should().BePositive();

            // Generic object comparison.
            uuid3.CompareTo((object) uuid3).Should().Be(0);
            uuid1.CompareTo((object) uuid3).Should().BeNegative();
            uuid3.CompareTo((object) uuid1).Should().BePositive();

            // Operator version.
#pragma warning disable CS1718
            (uuid1 < uuid1).Should().BeFalse();
            (uuid1 > uuid1).Should().BeFalse();
            (uuid1 <= uuid1).Should().BeTrue();
            (uuid1 >= uuid1).Should().BeTrue();
#pragma warning restore CS1718
            (uuid1 < uuid2).Should().BeTrue();
            (uuid2 < uuid3).Should().BeTrue();
            (uuid1 < uuid3).Should().BeTrue();
            (uuid1 <= uuid2).Should().BeTrue();
            (uuid2 <= uuid3).Should().BeTrue();
            (uuid1 <= uuid3).Should().BeTrue();
            (uuid1 > uuid2).Should().BeFalse();
            (uuid2 > uuid3).Should().BeFalse();
            (uuid1 > uuid3).Should().BeFalse();
            (uuid1 >= uuid2).Should().BeFalse();
            (uuid2 >= uuid3).Should().BeFalse();
            (uuid1 >= uuid3).Should().BeFalse();
        }

        [Theory]
        [InlineData("00000000-0000-0000-0000-000000000001")]
        [InlineData("00000000-0000-0000-0000-000000000010")]
        [InlineData("00000000-0000-0000-0000-000000000100")]
        [InlineData("00000000-0000-0000-0000-000000001000")]
        [InlineData("00000000-0000-0000-0000-000000010000")]
        [InlineData("00000000-0000-0000-0000-000000100000")]
        [InlineData("00000000-0000-0000-0000-000001000000")]
        [InlineData("00000000-0000-0000-0000-000010000000")]
        [InlineData("00000000-0000-0000-0000-000100000000")]
        [InlineData("00000000-0000-0000-0000-001000000000")]
        [InlineData("00000000-0000-0000-0000-010000000000")]
        [InlineData("00000000-0000-0000-0000-100000000000")]
        [InlineData("00000000-0000-0000-0001-000000000000")]
        [InlineData("00000000-0000-0000-0010-000000000000")]
        [InlineData("00000000-0000-0000-0100-000000000000")]
        [InlineData("00000000-0000-0000-1000-000000000000")]
        [InlineData("00000000-0000-0001-0000-000000000000")]
        [InlineData("00000000-0000-0010-0000-000000000000")]
        [InlineData("00000000-0000-0100-0000-000000000000")]
        [InlineData("00000000-0000-1000-0000-000000000000")]
        [InlineData("00000000-0001-0000-0000-000000000000")]
        [InlineData("00000000-0010-0000-0000-000000000000")]
        [InlineData("00000000-0100-0000-0000-000000000000")]
        [InlineData("00000000-1000-0000-0000-000000000000")]
        [InlineData("00000001-0000-0000-0000-000000000000")]
        [InlineData("00000010-0000-0000-0000-000000000000")]
        [InlineData("00000100-0000-0000-0000-000000000000")]
        [InlineData("00001000-0000-0000-0000-000000000000")]
        [InlineData("00010000-0000-0000-0000-000000000000")]
        [InlineData("00100000-0000-0000-0000-000000000000")]
        [InlineData("01000000-0000-0000-0000-000000000000")]
        [InlineData("10000000-0000-0000-0000-000000000000")]
        public void NonEmptyBitTest(string uuidString)
        {
            var uuid = new Uuid(uuidString);

            uuid.IsEmpty.Should().BeFalse();
            (uuid == Uuid.Empty).Should().BeFalse();
        }

        [Fact]
        public void EmptyTest()
        {
            var uuid = new Uuid("00000000-0000-0000-0000-000000000000");

            uuid.IsEmpty.Should().BeTrue();
            (uuid == Uuid.Empty).Should().BeTrue();
        }

        [Fact]
        public void NewRandom()
        {
            var uuid1 = Uuid.NewRandom();
            var uuid2 = Uuid.NewRandom();

            uuid1.Version.Should().Be(UuidVersion.Random);
            uuid2.Version.Should().Be(UuidVersion.Random);

            uuid1.Variant.Should().Be(UuidVariant.Rfc4122);
            uuid2.Variant.Should().Be(UuidVariant.Rfc4122);

            (uuid1 != uuid2).Should().BeTrue();
            (uuid1 != Uuid.Empty).Should().BeTrue();
        }

        [Fact]
        public void NewNameBasedV3()
        {
            var uuid1 = Uuid.NewNameBasedV3(Uuid.DnsNamespace, "Test");
            var uuid2 = Uuid.NewNameBasedV3(
                Uuid.DnsNamespace, new[] { (byte) 'T', (byte) 'e', (byte) 's', (byte) 't' }
            );

            uuid1.Version.Should().Be(UuidVersion.NameBasedMd5);
            uuid2.Version.Should().Be(UuidVersion.NameBasedMd5);

            uuid1.Variant.Should().Be(UuidVariant.Rfc4122);
            uuid2.Variant.Should().Be(UuidVariant.Rfc4122);

            (uuid1 == uuid2).Should().BeTrue();

            uuid1.ToString().Should().Be("5f210efc-8898-3b6d-baee-94274cd64adb");
        }

        [Fact]
        public void NewNameBasedV5()
        {
            var uuid1 = Uuid.NewNameBasedV5(Uuid.DnsNamespace, "Test");
            var uuid2 = Uuid.NewNameBasedV5(
                Uuid.DnsNamespace, new[] { (byte) 'T', (byte) 'e', (byte) 's', (byte) 't' }
            );

            uuid1.Version.Should().Be(UuidVersion.NameBasedSha1);
            uuid2.Version.Should().Be(UuidVersion.NameBasedSha1);

            uuid1.Variant.Should().Be(UuidVariant.Rfc4122);
            uuid2.Variant.Should().Be(UuidVariant.Rfc4122);

            (uuid1 == uuid2).Should().BeTrue();

            uuid1.ToString().Should().Be("c0400ee6-bc6e-583e-a1b5-67e698524de0");
        }

        [Fact]
        public void NewTimeBased()
        {
            var uuid1 = Uuid.NewTimeBased();
            var uuid2 = Uuid.NewTimeBased();

            uuid1.Version.Should().Be(UuidVersion.TimeBased);
            uuid2.Version.Should().Be(UuidVersion.TimeBased);

            uuid1.Variant.Should().Be(UuidVariant.Rfc4122);
            uuid2.Variant.Should().Be(UuidVariant.Rfc4122);

            (uuid1 != uuid2).Should().BeTrue();
            (uuid1 != Uuid.Empty).Should().BeTrue();
            (uuid2 != Uuid.Empty).Should().BeTrue();

            var bytes1 = uuid1.ToByteArray();
            var bytes2 = uuid2.ToByteArray();

            // Time mid should be equal.
            bytes1[4].Should().Be(bytes2[4]);
            bytes1[5].Should().Be(bytes2[5]);

            // Time mid should be equal.
            bytes1[6].Should().Be(bytes2[6]);
            bytes1[7].Should().Be(bytes2[7]);

            // Clock sequence should be equal, as values were generated by the same thread.
            bytes1[8].Should().Be(bytes2[8]);
            bytes1[9].Should().Be(bytes2[9]);
        }

        [Fact]
        public void PredefinedUuids()
        {
            Uuid.Empty.ToString().Should().Be("00000000-0000-0000-0000-000000000000");
            Uuid.DnsNamespace.ToString().Should().Be("6ba7b810-9dad-11d1-80b4-00c04fd430c8");
            Uuid.OidNamespace.ToString().Should().Be("6ba7b812-9dad-11d1-80b4-00c04fd430c8");
            Uuid.UrlNamespace.ToString().Should().Be("6ba7b811-9dad-11d1-80b4-00c04fd430c8");
            Uuid.X500Namespace.ToString().Should().Be("6ba7b814-9dad-11d1-80b4-00c04fd430c8");
        }

    }

}
