﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using JetBrains.Annotations;
using XploRe.Runtime;

namespace XploRe.Util;

/// <summary>
///     Represents a generic, immutable UUID that is binary compatible with <see cref="T:System.Guid" />.
///     <para>
///     The generation of UUIDs of all supported versions is thread-safe. Additionally, generation is lock-free
///     with the exception of one-time initialisation of resources.
///     </para>
/// </summary>
/// <remarks>
///     Instead of extending <see cref="T:System.Guid" />, this implementation provides high-performance operations
///     on the internal components of the UUID that are not exposed by <see cref="T:System.Guid" /> and would
///     otherwise require the allocation of a byte array for most operations. Due to binary compatibility with
///     <see cref="T:System.Guid" />, a <see cref="Uuid" /> can be marshalled into a <see cref="T:System.Guid" />
///     and vice-versa.
/// </remarks>
/// <see href="https://tools.ietf.org/html/rfc4122.html">RFC 4122: A Universally Unique IDentifier (UUID) URN Namespace</see>
[PublicAPI]
[StructLayout(LayoutKind.Sequential)]
public readonly struct Uuid
    : IComparable, IComparable<Uuid>, IEquatable<Uuid>
#if NETSTANDARD2_0
    , IFormattable
#endif
{

    /// <summary>
    ///     Empty <see cref="Uuid" /> instance whose bits are all set to zero.
    /// </summary>
    public static readonly Uuid Empty;

    /// <summary>
    ///     DNS namespace name-based UUIDs as defined by RFC 4122. 
    /// </summary>
    public static readonly Uuid DnsNamespace =
        new(0x6ba7b810, 0x9dad, 0x11d1, new byte[] { 0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8 });

    /// <summary>
    ///     URL namespace name-based UUIDs as defined by RFC 4122. 
    /// </summary>
    public static readonly Uuid UrlNamespace =
        new(0x6ba7b811, 0x9dad, 0x11d1, new byte[] { 0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8 });

    /// <summary>
    ///     OID namespace name-based UUIDs as defined by RFC 4122. 
    /// </summary>
    public static readonly Uuid OidNamespace =
        new(0x6ba7b812, 0x9dad, 0x11d1, new byte[] { 0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8 });

    /// <summary>
    ///     X.500 namespace name-based UUIDs as defined by RFC 4122. 
    /// </summary>
    public static readonly Uuid X500Namespace =
        new(0x6ba7b814, 0x9dad, 0x11d1, new byte[] { 0x80, 0xb4, 0x00, 0xc0, 0x4f, 0xd4, 0x30, 0xc8 });

    /// <summary>
    ///     The base offset date in UTC a UUID version 1 or version 2 timestamp starts at (the date on which the
    ///     Gregorian calendar was first adopted).
    /// </summary>
    private static readonly DateTimeOffset TimestampOffset =
        new(new DateTime(1582, 10, 15, 0, 0, 0, 0, DateTimeKind.Utc));


    #region Fields

    // Uses the exact same memory layout as System.Guid for binary compatibility.
    // Naming is based on the WinAPI GUID structure and agnostic of the UUID version.

    private readonly int _data1;
    private readonly short _data2;
    private readonly short _data3;
    private readonly byte _data4_0;
    private readonly byte _data4_1;
    private readonly byte _data4_2;
    private readonly byte _data4_3;
    private readonly byte _data4_4;
    private readonly byte _data4_5;
    private readonly byte _data4_6;
    private readonly byte _data4_7;

    #endregion


    /// <summary>
    ///     Determines whether the UUID is empty, i.e. equal to the <see cref="Empty" /> instance with all bits set
    ///     to zero.
    /// </summary>
    public bool IsEmpty
        => (_data1 | (ushort) _data2 | (ushort) _data3 |
            _data4_0 | _data4_1 | _data4_2 | _data4_3 | _data4_4 | _data4_5 | _data4_6 | _data4_7) == 0;

    /// <summary>
    ///     Uniform resource name (URN) for this <see cref="Uuid" />. 
    /// </summary>
    public string Urn => $"urn:uuid:{ToString("D")}";

    /// <summary>
    ///     The version of the UUID represented by this instance. Only meaningful if the variant is
    ///     <see cref="UuidVariant.Rfc4122" />. May yield a version that is not defined by <see cref="UuidVersion" />
    ///     for future expansion (the numeric value always corresponds to the embedded version number).
    /// </summary>
    public UuidVersion Version => (UuidVersion) (_data3 >> 12);

    /// <summary>
    ///     The variant of the UUID represented by this instance.
    /// </summary>
    public UuidVariant Variant
    {
        get {
            if ((_data4_0 & 0x80) == 0x00) {
                // Variant 0 (all bits zero), used by the obsolete Apollo Network Computing System 1.5 UUID format.
                return UuidVariant.Ncs;
            }

            if ((_data4_0 & 0xc0) == 0x80) {
                // Only first bit set.
                return UuidVariant.Rfc4122;
            }

            if ((_data4_0 & 0xe0) == 0xc0) {
                // First two bits set.
                return UuidVariant.Microsoft;
            }

            // May be extended in the future with additional variants.
            return UuidVariant.Future;
        }
    }

    /// <summary>
    ///     Yields the regular byte order of the UUID for storage and transmission depending on the variant flag.
    /// </summary>
    /// <remarks>
    ///     Traditionally, <see cref="UuidVariant.Microsoft" /> variant UUIDs use little-endian encoding for the
    ///     first three data fields (32-bit, 16-bit and 16-bit, respectively). Other UUIDs default to network byte
    ///     order, i.e. big-endian. 
    /// </remarks>
    public ByteOrder VariantByteOrder
        => Variant == UuidVariant.Microsoft ? ByteOrder.LittleEndian : ByteOrder.BigEndian;

    /// <summary>
    ///     Initialises a new <see cref="Uuid" /> instance with the provided integers and bytes.
    /// </summary>
    /// <param name="data1">The first 4 bytes of the UUID.</param>
    /// <param name="data2">The next 2 bytes of the UUID.</param>
    /// <param name="data3">The next 2 bytes of the UUID.</param>
    /// <param name="data4_0">The next byte of the UUID.</param>
    /// <param name="data4_1">The next byte of the UUID.</param>
    /// <param name="data4_2">The next byte of the UUID.</param>
    /// <param name="data4_3">The next byte of the UUID.</param>
    /// <param name="data4_4">The next byte of the UUID.</param>
    /// <param name="data4_5">The next byte of the UUID.</param>
    /// <param name="data4_6">The next byte of the UUID.</param>
    /// <param name="data4_7">The next byte of the UUID.</param>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public Uuid(
        int data1, short data2, short data3,
        byte data4_0, byte data4_1, byte data4_2, byte data4_3,
        byte data4_4, byte data4_5, byte data4_6, byte data4_7)
    {
        _data1 = data1;
        _data2 = data2;
        _data3 = data3;
        _data4_0 = data4_0;
        _data4_1 = data4_1;
        _data4_2 = data4_2;
        _data4_3 = data4_3;
        _data4_4 = data4_4;
        _data4_5 = data4_5;
        _data4_6 = data4_6;
        _data4_7 = data4_7;
    }

    /// <summary>
    ///     Initialises a new <see cref="Uuid" /> instance with the provided integers and bytes.
    /// </summary>
    /// <param name="data1">The first 4 bytes of the UUID.</param>
    /// <param name="data2">The next 2 bytes of the UUID.</param>
    /// <param name="data3">The next 2 bytes of the UUID.</param>
    /// <param name="data4_0">The next byte of the UUID.</param>
    /// <param name="data4_1">The next byte of the UUID.</param>
    /// <param name="data4_2">The next byte of the UUID.</param>
    /// <param name="data4_3">The next byte of the UUID.</param>
    /// <param name="data4_4">The next byte of the UUID.</param>
    /// <param name="data4_5">The next byte of the UUID.</param>
    /// <param name="data4_6">The next byte of the UUID.</param>
    /// <param name="data4_7">The next byte of the UUID.</param>
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public Uuid(
        uint data1, ushort data2, ushort data3,
        byte data4_0, byte data4_1, byte data4_2, byte data4_3,
        byte data4_4, byte data4_5, byte data4_6, byte data4_7)
    {
        _data1 = (int) data1;
        _data2 = (short) data2;
        _data3 = (short) data3;
        _data4_0 = data4_0;
        _data4_1 = data4_1;
        _data4_2 = data4_2;
        _data4_3 = data4_3;
        _data4_4 = data4_4;
        _data4_5 = data4_5;
        _data4_6 = data4_6;
        _data4_7 = data4_7;
    }

    /// <summary>
    ///     Initialises a new <see cref="Uuid" /> instance with the provided integers and bytes.
    /// </summary>
    /// <param name="data1">The first 4 bytes of the UUID.</param>
    /// <param name="data2">The next 2 bytes of the UUID.</param>
    /// <param name="data3">The next 2 bytes of the UUID.</param>
    /// <param name="data4">The next 8 bytes of the UUID.</param>
    public Uuid(int data1, short data2, short data3, byte[] data4)
    {
        if (data4 == null) {
            throw new ArgumentNullException(nameof(data4));
        }

        if (data4.Length != 8) {
            throw new ArgumentException(
                $"Bytes array must hold exactly 8 elements, {data4.Length} found.",
                nameof(data4)
                );
        }

        _data1 = data1;
        _data2 = data2;
        _data3 = data3;
        _data4_0 = data4[0];
        _data4_1 = data4[1];
        _data4_2 = data4[2];
        _data4_3 = data4[3];
        _data4_4 = data4[4];
        _data4_5 = data4[5];
        _data4_6 = data4[6];
        _data4_7 = data4[7];
    }

    /// <summary>
    ///     Initialises a new <see cref="Uuid" /> instance with the provided integers and bytes.
    /// </summary>
    /// <param name="data1">The first 4 bytes of the UUID.</param>
    /// <param name="data2">The next 2 bytes of the UUID.</param>
    /// <param name="data3">The next 2 bytes of the UUID.</param>
    /// <param name="data4">The next 8 bytes of the UUID.</param>
    public Uuid(uint data1, ushort data2, ushort data3, byte[] data4)
    {
        if (data4 == null) {
            throw new ArgumentNullException(nameof(data4));
        }

        if (data4.Length != 8) {
            throw new ArgumentException(
                $"Bytes array must hold exactly 8 elements, {data4.Length} found.",
                nameof(data4)
                );
        }

        _data1 = (int) data1;
        _data2 = (short) data2;
        _data3 = (short) data3;
        _data4_0 = data4[0];
        _data4_1 = data4[1];
        _data4_2 = data4[2];
        _data4_3 = data4[3];
        _data4_4 = data4[4];
        _data4_5 = data4[5];
        _data4_6 = data4[6];
        _data4_7 = data4[7];
    }

    /// <inheritdoc />
    /// <summary>
    ///     Initialises a new <see cref="Uuid" /> instance with the provided bytes. Bytes are expected in
    ///     little-endian byte order for drop-in compatibility with <see cref="Guid(byte[])" />.
    /// </summary>
    /// <param name="bytes">The 16 bytes of the UUID.</param>
    [SuppressMessage("ReSharper", "IntroduceOptionalParameters.Global")]
    public Uuid(byte[] bytes) : this(bytes, ByteOrder.LittleEndian)
    {
    }

    /// <summary>
    ///     Initialises a new <see cref="Uuid" /> instance with the provided bytes.
    /// </summary>
    /// <param name="bytes">The 16 bytes of the UUID.</param>
    /// <param name="byteOrder">
    ///     The byte order of the first three data fields (32-bit, 16-bit and 16-bit, respectively) represented by
    ///     the first 8 bytes in the provided bytes array.
    /// </param>
    public Uuid(byte[] bytes, ByteOrder byteOrder)
    {
        if (bytes == null) {
            throw new ArgumentNullException(nameof(bytes));
        }

        if (bytes.Length != 16) {
            throw new ArgumentException(
                $"Bytes array must hold exactly 16 elements, {bytes.Length} found.",
                nameof(bytes)
                );
        }

        if (byteOrder.IsNetworkOrder()) {
            _data1 = (bytes[0] << 24) | (bytes[1] << 16) | (bytes[2] << 8) | bytes[3];
            _data2 = (short) ((bytes[4] << 8) | bytes[5]);
            _data3 = (short) ((bytes[6] << 8) | bytes[7]);
        }
        else {
            _data1 = (bytes[3] << 24) | (bytes[2] << 16) | (bytes[1] << 8) | bytes[0];
            _data2 = (short) ((bytes[5] << 8) | bytes[4]);
            _data3 = (short) ((bytes[7] << 8) | bytes[6]);
        }

        _data4_0 = bytes[8];
        _data4_1 = bytes[9];
        _data4_2 = bytes[10];
        _data4_3 = bytes[11];
        _data4_4 = bytes[12];
        _data4_5 = bytes[13];
        _data4_6 = bytes[14];
        _data4_7 = bytes[15];
    }

    /// <summary>
    ///     Initialises a new <see cref="Uuid" /> instance with the value represented by the provided string.
    /// </summary>
    /// <param name="uuid">
    ///     UUID value representation to initialise new instance with. Supports the same formats as
    ///     <see cref="Guid(string)" />
    /// </param>
    public Uuid(string uuid)
    {
        // Parse using Guid and convert to UUID. The additional conversion overhead is minimal compared to the
        // rather complex parsing process.
        this = new Guid(uuid);
    }

    /// <summary>
    ///     Returns the hash code for this instance. The hash code is equal to the hash code returned by 
    ///     <see cref="Guid.GetHashCode()" /> for the same UUID and depends on the .NET runtime.
    /// </summary>
    /// <returns>Hash code for this instance.</returns>
    [Pure]
    public override int GetHashCode()
    {
        // Use CLR implementation as CoreCLR and the .NET CLR have different implementations.
        return ((Guid) this).GetHashCode();
    }

    /// <summary>
    ///     Returns a 16-element byte array that contains the UUID in little-endian byte order. The byte-order is
    ///     identical to the byte array returned by <see cref="Guid.ToByteArray()" /> for drop-in compatibility. If
    ///     interoperability with other platforms is required, network byte order should be used for transmission
    ///     and storage of UUIDs, which is achieved by instead using <see cref="ToByteArray(ByteOrder)" /> with the
    ///     <see cref="ByteOrder.BigEndian" /> argument.
    /// </summary>
    /// <returns>A 16-element byte array that contains the UUID in little-endian byte order.</returns>
    [Pure]
    public byte[] ToByteArray()
    {
        // ReSharper disable once IntroduceOptionalParameters.Global
        return ToByteArray(ByteOrder.LittleEndian);
    }

    /// <summary>
    ///     Returns a 16-element byte array that contains the UUID.
    /// </summary>
    /// <param name="order">
    ///     The requested byte-order of the first three data fields (32-bit, 16-bit and 16-bit, respectively) that
    ///     are stored by the first 8 bytes in the returned array. For interoperability with other systems and to
    ///     comply with RFC 4122, use <see cref="ByteOrder.BigEndian" /> to transmit and store UUIDs. The default
    ///     byte order used by <see cref="Guid" /> is <see cref="ByteOrder.LittleEndian" />. 
    /// </param>
    /// <returns>A 16-element byte array that contains the UUID in the requested byte-order.</returns>
    [Pure]
    public byte[] ToByteArray(ByteOrder order)
    {
        var result = new byte[16];

        if (order.IsNetworkOrder()) {
            result[0] = (byte) (_data1 >> 24);
            result[1] = (byte) (_data1 >> 16);
            result[2] = (byte) (_data1 >> 8);
            result[3] = (byte) (_data1);
            result[4] = (byte) (_data2 >> 8);
            result[5] = (byte) (_data2);
            result[6] = (byte) (_data3 >> 8);
            result[7] = (byte) (_data3);
        }
        else {
            result[0] = (byte) (_data1);
            result[1] = (byte) (_data1 >> 8);
            result[2] = (byte) (_data1 >> 16);
            result[3] = (byte) (_data1 >> 24);
            result[4] = (byte) (_data2);
            result[5] = (byte) (_data2 >> 8);
            result[6] = (byte) (_data3);
            result[7] = (byte) (_data3 >> 8);
        }

        result[8] = _data4_0;
        result[9] = _data4_1;
        result[10] = _data4_2;
        result[11] = _data4_3;
        result[12] = _data4_4;
        result[13] = _data4_5;
        result[14] = _data4_6;
        result[15] = _data4_7;

        return result;
    }

    /// <summary>
    ///     Returns a new <see cref="Guid" /> instance that represents the same UUID.
    /// </summary>
    /// <returns>New <see cref="Guid" /> instance that represents the same UUID.</returns>
    [Pure]
    public Guid ToGuid()
    {
        var handle = GCHandle.Alloc(this, GCHandleType.Pinned);
        var guid = Marshal.PtrToStructure<Guid>(handle.AddrOfPinnedObject());
        handle.Free();

        return guid;
    }

    /// <summary>
    ///     Returns a string representation of the UUID in registry format.
    /// </summary>
    /// <returns>String representation of the UUID in registry format.</returns>
    [Pure]
    public override string ToString()
    {
        return ToString("D");
    }

    /// <summary>
    ///     Returns a string representation of the UUID according to the provided format specifier.
    ///     <para>
    ///     See documentation on <see cref="Guid.ToString(string)" /> for details on the format specifiers.
    ///     </para>
    /// </summary>
    /// <param name="format">
    ///     A single format specifier that indicates how the UUID should be formatted. See the documentation on
    ///     <see cref="Guid.ToString(string)" /> for a list of supported specifiers.
    /// </param>
    /// <returns>String representation of the UUID according to the provided format specifier.</returns>
    [Pure]
    public string ToString(string? format)
    {
        return ToGuid().ToString(format!);
    }

#if NETSTANDARD2_0

    /// <inheritdoc />
    /// <summary>
    ///     Returns a string representation of the UUID according to the provided format specifier and
    ///     culture-specific format information.
    /// </summary>
    /// <param name="format">
    ///     A single format specifier that indicates how the UUID should be formatted. See the documentation on
    ///     <see cref="Guid.ToString(string,IFormatProvider)" /> for a list of supported specifiers.
    /// </param>
    /// <param name="formatProvider"></param>
    /// <returns>String representation of the UUID according to the provided format specifier.</returns>
    [Pure]
    public string ToString(string? format, IFormatProvider formatProvider)
    {
        return ToGuid().ToString(format!, formatProvider);
    }

#endif

    /// <summary>
    ///     Yields a <see cref="Guid" /> instance that represents the same UUID.
    /// </summary>
    /// <param name="uuid">The <see cref="Uuid" /> instance to convert.</param>
    /// <returns>New <see cref="Guid" /> instance that represents the same UUID.</returns>
    [Pure]
    public static implicit operator Guid(Uuid uuid)
    {
        return uuid.ToGuid();
    }

    /// <summary>
    ///     Yields a <see cref="Uuid" /> instance that represents the same GUID.
    /// </summary>
    /// <param name="guid">The <see cref="Guid" /> instance to convert.</param>
    /// <returns>New <see cref="Uuid" /> instance that represents the same GUID.</returns>
    [Pure]
    public static implicit operator Uuid(Guid guid)
    {
        return guid.ToUuid();
    }

    /// <summary>
    ///     Yields a <see cref="Uuid" /> instance that represents the textual UUID.
    /// </summary>
    /// <param name="uuid">UUID string to convert.</param>
    /// <returns>New <see cref="Uuid" /> instance that represents the textual UUID.</returns>
    [Pure]
    public static explicit operator Uuid(string uuid)
    {
        return new Uuid(uuid);
    }


    #region Comparison

    /// <summary>
    ///     Compares this <see cref="Uuid" /> instance with another <see cref="Uuid" /> instance and returns an
    ///     integer value that indicates whether this instance precedes, follows, or occurs at the same position in
    ///     the sort order as the other instance.
    /// </summary>
    /// <param name="obj">Another <see cref="Uuid" /> instance to compare this instance to.</param>
    /// <returns>
    ///     A value less than zero, if this instance precedes <paramref name="obj" />, zero, if this instance occurs
    ///     at the same position as <paramref name="obj" />, or greater than zero, if this instance follows
    ///     <paramref name="obj" /> in the sort order.
    /// </returns>
    /// <exception cref="ArgumentException"><paramref name="obj" /> is not an <see cref="Uuid" /> instance.</exception>
    [Pure]
    public int CompareTo(object obj)
    {
        return obj switch {
            null      => 1,
            Uuid uuid => CompareTo(uuid),
            _         => throw new ArgumentException($"Argument for comparison must be an instance of type '{typeof(Uuid)}'.", nameof(obj)),
        };

    }

    /// <summary>
    ///     Compares this <see cref="Uuid" /> instance with another <see cref="Uuid" /> instance and returns an
    ///     integer value that indicates whether this instance precedes, follows, or occurs at the same position in
    ///     the sort order as the other instance.
    /// </summary>
    /// <param name="uuid">Another <see cref="Uuid" /> instance to compare this instance to.</param>
    /// <returns>
    ///     A value less than zero, if this instance precedes <paramref name="uuid" />, zero, if this instance
    ///     occurs at the same position as <paramref name="uuid" />, or greater than zero, if this instance follows
    ///     <paramref name="uuid" /> in the sort order.
    /// </returns>
    [Pure]
    public int CompareTo(Uuid uuid)
    {
        if (_data1 != uuid._data1) {
            if (_data1 < uuid._data1) {
                return -1;
            }

            return 1;
        }

        if (_data2 != uuid._data2) {
            if (_data2 < uuid._data2) {
                return -1;
            }

            return 1;
        }

        if (_data3 != uuid._data3) {
            if (_data3 < uuid._data3) {
                return -1;
            }

            return 1;
        }

        if (_data4_0 != uuid._data4_0) {
            if (_data4_0 < uuid._data4_0) {
                return -1;
            }

            return 1;
        }

        if (_data4_1 != uuid._data4_1) {
            if (_data4_1 < uuid._data4_1) {
                return -1;
            }

            return 1;
        }

        if (_data4_2 != uuid._data4_2) {
            if (_data4_2 < uuid._data4_2) {
                return -1;
            }

            return 1;
        }

        if (_data4_3 != uuid._data4_3) {
            if (_data4_3 < uuid._data4_3) {
                return -1;
            }

            return 1;
        }

        if (_data4_4 != uuid._data4_4) {
            if (_data4_4 < uuid._data4_4) {
                return -1;
            }

            return 1;
        }

        if (_data4_5 != uuid._data4_5) {
            if (_data4_5 < uuid._data4_5) {
                return -1;
            }

            return 1;
        }

        if (_data4_6 != uuid._data4_6) {
            if (_data4_6 < uuid._data4_6) {
                return -1;
            }

            return 1;
        }

        if (_data4_7 != uuid._data4_7) {
            if (_data4_7 < uuid._data4_7) {
                return -1;
            }

            return 1;
        }

        return 0;
    }

    /// <summary>
    ///     Determines whether an <see cref="Uuid" /> instance precedes another <see cref="Uuid" /> instance in
    ///     the sort order.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if <paramref name="a" /> precedes <paramref name="b" /> in the sort order, otherwise 
    ///     <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator <(Uuid a, Uuid b)
    {
        return a.CompareTo(b) < 0;
    }

    /// <summary>
    ///     Determines whether an <see cref="Uuid" /> instance follows another <see cref="Uuid" /> instance in the
    ///     sort order.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if <paramref name="a" /> follows <paramref name="b" /> in the sort order, otherwise
    ///     <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator >(Uuid a, Uuid b)
    {
        return b.CompareTo(a) < 0;
    }

    /// <summary>
    ///     Determines whether an <see cref="Uuid" /> instance precedes or occurs at the same position as another
    ///     <see cref="Uuid" /> instance in the sort order.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if <paramref name="a" /> precedes or occurs at the same position as <paramref name="b" />
    ///     in the sort order, otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator <=(Uuid a, Uuid b)
    {
        return !(b.CompareTo(a) < 0);
    }

    /// <summary>
    ///     Determines whether an <see cref="Uuid" /> instance follows or occurs at the same position as another
    ///     <see cref="Uuid" /> instance in the sort order.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if <paramref name="a" /> follows or occurs at the same position as <paramref name="b" />
    ///     in the sort order, otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator >=(Uuid a, Uuid b)
    {
        return !(a.CompareTo(b) < 0);
    }

    #endregion


    #region Equality

    /// <inheritdoc />
    /// <summary>
    ///     Indicates whether this <see cref="Uuid" /> instance is equal to another <see cref="Uuid" /> instance.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if <paramref name="obj" /> and this instance both represent a <see cref="Uuid" /> with the 
    ///     same value, otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public override bool Equals(object obj)
    {
        if (obj is Uuid uuid) {
            return uuid._data1 == _data1 &&
                   uuid._data2 == _data2 &&
                   uuid._data3 == _data3 &&
                   uuid._data4_0 == _data4_0 &&
                   uuid._data4_1 == _data4_1 &&
                   uuid._data4_2 == _data4_2 &&
                   uuid._data4_3 == _data4_3 &&
                   uuid._data4_4 == _data4_4 &&
                   uuid._data4_5 == _data4_5 &&
                   uuid._data4_6 == _data4_6 &&
                   uuid._data4_7 == _data4_7;
        }

        return false;
    }

    /// <inheritdoc />
    /// <summary>
    ///     Indicates whether this <see cref="Uuid" /> instance is equal to another <see cref="Uuid" /> instance.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if the provided <paramref name="uuid" /> and this instance both represent the same value,
    ///     otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public bool Equals(Uuid uuid)
    {
        return uuid._data1 == _data1 &&
               uuid._data2 == _data2 &&
               uuid._data3 == _data3 &&
               uuid._data4_0 == _data4_0 &&
               uuid._data4_1 == _data4_1 &&
               uuid._data4_2 == _data4_2 &&
               uuid._data4_3 == _data4_3 &&
               uuid._data4_4 == _data4_4 &&
               uuid._data4_5 == _data4_5 &&
               uuid._data4_6 == _data4_6 &&
               uuid._data4_7 == _data4_7;
    }

    /// <summary>
    ///     Tests whether two <see cref="Uuid" /> instances are equal.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if both <see cref="Uuid" /> instances represent the same value, otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator ==(Uuid a, Uuid b)
    {
        return a._data1 == b._data1 &&
               a._data2 == b._data2 &&
               a._data3 == b._data3 &&
               a._data4_0 == b._data4_0 &&
               a._data4_1 == b._data4_1 &&
               a._data4_2 == b._data4_2 &&
               a._data4_3 == b._data4_3 &&
               a._data4_4 == b._data4_4 &&
               a._data4_5 == b._data4_5 &&
               a._data4_6 == b._data4_6 &&
               a._data4_7 == b._data4_7;
    }

    /// <summary>
    ///     Tests whether two <see cref="Uuid" /> instances are not equal.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if the <see cref="Uuid" /> instances do not represent the same value, otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator !=(Uuid a, Uuid b)
    {
        return a._data1 != b._data1 ||
               a._data2 != b._data2 ||
               a._data3 != b._data3 ||
               a._data4_0 != b._data4_0 ||
               a._data4_1 != b._data4_1 ||
               a._data4_2 != b._data4_2 ||
               a._data4_3 != b._data4_3 ||
               a._data4_4 != b._data4_4 ||
               a._data4_5 != b._data4_5 ||
               a._data4_6 != b._data4_6 ||
               a._data4_7 != b._data4_7;
    }

    // Specific Uuid and Guid (in)equality operators are required to avoid ambiguity due to the implicit conversion
    // operators.

    /// <summary>
    ///     Tests whether a <see cref="Uuid" /> instance and a <see cref="Guid" /> instance are equal.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if both <paramref name="uuid" /> and <paramref name="guid" /> represent the same value,
    ///     otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator ==(Uuid uuid, Guid guid)
    {
        // Use equality operator from Guid as it internally uses unsafe operations on CoreCLR for efficiency.
        return (Guid) uuid == guid;
    }

    /// <summary>
    ///     Tests whether a <see cref="Uuid" /> instance and a <see cref="Guid" /> instance are not equal.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if the provided <paramref name="uuid" /> and <paramref name="guid" /> do not represent the
    ///     same value, otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator !=(Uuid uuid, Guid guid)
    {
        // Use inequality operator from Guid as it internally uses unsafe operations on CoreCLR for efficiency.
        return (Guid) uuid != guid;
    }

    /// <summary>
    ///     Tests whether a <see cref="Guid" /> instance and a <see cref="Uuid" /> instance are equal.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if both <paramref name="guid" /> and <paramref name="uuid" /> represent the same value,
    ///     otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator ==(Guid guid, Uuid uuid)
    {
        // Use equality operator from Guid as it internally uses unsafe operations on CoreCLR for efficiency.
        return guid == (Guid) uuid;
    }

    /// <summary>
    ///     Tests whether a <see cref="Guid" /> instance and a <see cref="Uuid" /> instance are not equal.
    /// </summary>
    /// <returns>
    ///     <c>true</c>, if the provided <paramref name="guid" /> and <paramref name="uuid" /> do not represent the
    ///     same value, otherwise <c>false</c>.
    /// </returns>
    [Pure]
    public static bool operator !=(Guid guid, Uuid uuid)
    {
        // Use inequality operator from Guid as it internally uses unsafe operations on CoreCLR for efficiency.
        return guid != (Guid) uuid;
    }

    #endregion


    /// <summary>
    ///     Lazily retrieves the hardware node ID of the host system. If not available or supported, a random ID
    ///     with the multicast bit set is generated instead. Access to this property is thread-safe.
    /// </summary>
    private static byte[] LocalNode
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        // Local implementation never returns null.
        get => _localNode.Value!;
    }
    
    private static readonly Lazy<byte[]> _localNode = new(
        () => {
            // Currently, no hardware nodes are supported. Use a randomly generated node instead.
            var bytes = new byte[6];

            RngProvider.GetBytes(bytes);

            // Set the multicast bit as recommended by RFC 4122 to distinguish between hardware nodes and randomly
            // generated nodes.
            bytes[0] |= 0x01;

            return bytes;
        }, LazyThreadSafetyMode.ExecutionAndPublication
        );

    /// <summary>
    ///     Clock sequence to ensure generation of unique UUIDs if multiple instances are running simultaneously.
    ///     Generated per thread (i.e. treats threads as different, concurrently running generator instances) to
    ///     avoid the need for locks on the cached timestamp.
    /// </summary>
    private static byte[] LocalClockSequence
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        // Local implementation never returns null.
        get => _localClockSequence.Value!;
    }
    
    private static readonly ThreadLocal<byte[]> _localClockSequence = new(
        () => {
            var bytes = new byte[2];

            // Instead of a persistent storage, use a random number as a recommended alternative by RFC 4122.
            RngProvider.GetBytes(bytes);

            return bytes;
        });

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static void UpdateLocalClockSequence()
    {
        var bytes = LocalClockSequence;
        var sequence = (ushort) ((bytes[1] << 8) | bytes[0]);

        unchecked {
            sequence++;
        }

        bytes[0] = (byte) (sequence);
        bytes[1] = (byte) (sequence >> 8);
    }

    /// <summary>
    ///     The last issued timestamp. Cached per thread separately to avoid locks.
    /// </summary>
    private static readonly ThreadLocal<ulong> LastTimestamp = new();

    /// <summary>
    ///     Cached UTC ticks value of <see cref="TimestampOffset" />.
    /// </summary>
    private static readonly ulong TimestampTicksOffset = (ulong) TimestampOffset.UtcTicks;

    /// <summary>
    ///     Returns a new <see cref="Uuid" /> version 1 (RFC 4122 variant) instance with a time-based value, created
    ///     using a 48-bit node ID (randomly generated), a 60-bit timestamp in 100 ns intervals since the Gregorian
    ///     calendar was adopted, and a 14-bit clock sequence that is randomly generated per process/thread.
    /// </summary>
    /// <returns>A new version 1 <see cref="Uuid" /> instance based on the current time.</returns>
    [Pure]
    public static Uuid NewTimeBased()
    {
        // Get timestamp in 10e-7 (100 ns) units.
        var timestamp = ((ulong) DateTimeOffset.UtcNow.UtcTicks - TimestampTicksOffset)
                      / (TimeSpan.TicksPerMillisecond / 10000UL);
        // Last generated timestamp is a thread-local value, no lock needed.
        var lastTimestamp = LastTimestamp.Value;

        if (timestamp < lastTimestamp) {
            // Clock has gone backwards, update clock sequence.
            UpdateLocalClockSequence();
        }
        else if (timestamp == lastTimestamp) {
            // Adjust for insufficient clock resolution.
            timestamp = lastTimestamp + 1;
        }

        LastTimestamp.Value = timestamp;

        var timeLow = unchecked((int) timestamp);
        var timeMid = unchecked((short) (timestamp >> 32));
        var timeHiV = unchecked((short) (timestamp >> 48));

        // Store clock sequence and append node.
        var bytes = new byte[8];
        Array.Copy(LocalClockSequence, 0, bytes, 0, 2);
        Array.Copy(LocalNode, 0, bytes, 2, 6);

        // Set version to 1.
        timeHiV = (short) ((timeHiV & 0x1fff) | 0x1000);

        // Set variant to RFC 4122.
        bytes[0] = (byte) ((bytes[0] & 0xbf) | 0x80);

        return new Uuid(timeLow, timeMid, timeHiV, bytes);
    }

    /// <summary>
    ///     Lazily initialised MD5 hash provider.
    /// </summary>
    private static MD5 Md5
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        get => _md5.Value!;
    }

    private static readonly Lazy<MD5> _md5 = new(MD5.Create, LazyThreadSafetyMode.ExecutionAndPublication);

    /// <summary>
    ///     Returns a new <see cref="Uuid" /> version 3 (RFC 4122 variant) instance with a name-based value, created
    ///     from the MD5 hash of a given namespace UUID and an arbitrary name string.
    ///     <para>
    ///     The same namespace and name combination will always yield the same UUID.
    ///     </para>
    /// </summary>
    /// <returns>A new version 3 <see cref="Uuid" /> instance based on the provided namespace and name.</returns>
    /// <exception cref="ArgumentException">Failed to retrieve byte array from provided name.</exception>
    /// <exception cref="RuntimeException">MD5 initialisation or computation failed.</exception>
    [Pure]
    public static Uuid NewNameBasedV3(Uuid @namespace, string name)
    {
        return NewNameBased(Md5, @namespace, name, (int) UuidVersion.NameBasedMd5);
    }

    /// <summary>
    ///     Returns a new <see cref="Uuid" /> version 3 (RFC 4122 variant) instance with a name-based value, created
    ///     from the MD5 hash of a given namespace UUID and an arbitrary name byte array.
    ///     <para>
    ///     The same namespace and name combination will always yield the same UUID.
    ///     </para>
    /// </summary>
    /// <returns>A new version 3 <see cref="Uuid" /> instance based on the provided namespace and name.</returns>
    /// <exception cref="ArgumentException">Failed to retrieve byte array from provided name.</exception>
    /// <exception cref="RuntimeException">MD5 initialisation or computation failed.</exception>
    [Pure]
    public static Uuid NewNameBasedV3(Uuid @namespace, byte[] name)
    {
        if (name == null) {
            throw new ArgumentNullException(nameof(name));
        }

        return NewNameBased(Md5, @namespace, name, (int) UuidVersion.NameBasedMd5);
    }

    /// <summary>
    ///     Lazily initialised SHA-1 hash provider. Value is always non-null; throws if initialisation fails.
    /// </summary>
    private static SHA1 Sha1
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        get => _sha1.Value!; 
    }

    private static readonly Lazy<SHA1> _sha1 = new(
        () => {
            var sha1 = SHA1.Create();

            if (sha1 == null) {
                throw new RuntimeException("Failed to instantiate SHA-1.");
            }

            return sha1;
        }, LazyThreadSafetyMode.ExecutionAndPublication
        );

    /// <summary>
    ///     Returns a new <see cref="Uuid" /> version 5 (RFC 4122 variant) instance with a name-based value, created
    ///     from the SHA-1 hash of a given namespace UUID and an arbitrary name string.
    ///     <para>
    ///     The same namespace and name combination will always yield the same UUID.
    ///     </para>
    /// </summary>
    /// <returns>A new version 5 <see cref="Uuid" /> instance based on the provided namespace and name.</returns>
    /// <exception cref="ArgumentException">Failed to retrieve byte array from provided name.</exception>
    /// <exception cref="RuntimeException">SHA-1 initialisation or computation failed.</exception>
    [Pure]
    public static Uuid NewNameBasedV5(Uuid @namespace, string name)
    {
        return NewNameBased(Sha1, @namespace, name, (int) UuidVersion.NameBasedSha1);
    }

    /// <summary>
    ///     Returns a new <see cref="Uuid" /> version 5 (RFC 4122 variant) instance with a name-based value, created
    ///     from the SHA-1 hash of a given namespace UUID and an arbitrary name byte array.
    ///     <para>
    ///     The same namespace and name combination will always yield the same UUID.
    ///     </para>
    /// </summary>
    /// <returns>A new version 5 <see cref="Uuid" /> instance based on the provided namespace and name.</returns>
    /// <exception cref="ArgumentException">Failed to retrieve byte array from provided name.</exception>
    /// <exception cref="RuntimeException">SHA-1 initialisation or computation failed.</exception>
    [Pure]
    public static Uuid NewNameBasedV5(Uuid @namespace, byte[] name)
    {
        if (name == null) {
            throw new ArgumentNullException(nameof(name));
        }

        return NewNameBased(Sha1, @namespace, name, (int) UuidVersion.NameBasedSha1);
    }

    [Pure]
    private static Uuid NewNameBased(
        HashAlgorithm algorithm,
        Uuid @namespace, string name, int version)
    {
        if (name == null) {
            throw new ArgumentNullException(nameof(name));
        }

        return NewNameBased(algorithm, @namespace, Encoding.UTF8.GetBytes(name), version);
    }

    [Pure]
    private static Uuid NewNameBased(
        HashAlgorithm algorithm,
        Uuid @namespace, byte[] nameBytes, int version)
    {
        var nsBytes = @namespace.ToByteArray(ByteOrder.BigEndian);

        var sourceBytes = new byte[nsBytes.Length + nameBytes.Length];
        Array.Copy(nsBytes, sourceBytes, nsBytes.Length);
        Array.Copy(nameBytes, 0, sourceBytes, nsBytes.Length, nameBytes.Length);

        var hash = algorithm.ComputeHash(sourceBytes);

        var bytes = new byte[16];
        Array.Copy(hash, bytes, bytes.Length);

        // Set version.
        bytes[6] = (byte) ((bytes[6] & 0x0f) | (byte) (version << 4));

        // Set variant to RFC 4122.
        bytes[8] = (byte) ((bytes[8] & 0xbf) | 0x80);

        return new Uuid(bytes, ByteOrder.BigEndian);
    }
    
    /// <summary>
    ///     Lazily initialised cryptographically strong random bytes provider.
    /// </summary>
    private static RandomNumberGenerator RngProvider => _rngProvider.Value!;
    
    private static readonly Lazy<RandomNumberGenerator> _rngProvider = new(
        () => {
            var rng = RandomNumberGenerator.Create();

            if (rng == null) {
                throw new RuntimeException("Failed to instantiate RNG.");
            }

            return rng;
        }, LazyThreadSafetyMode.ExecutionAndPublication
        );

    /// <summary>
    ///     Returns a new <see cref="Uuid" /> version 4 (RFC 4122 variant) instance with a random value, based on a
    ///     cryptographically strong sequence of random bytes, utilising the default system-wide configured
    ///     <see cref="RandomNumberGenerator" />.
    /// </summary>
    /// <returns>A new version 4 <see cref="Uuid" /> instance with a random value.</returns>
    /// <exception cref="RuntimeException">RNG initialisation failed.</exception>
    [Pure]
    public static Uuid NewRandom()
    {
        var bytes = new byte[16];

        RngProvider.GetBytes(bytes);

        // Set version to 4.
        bytes[6] = (byte) ((bytes[6] & 0x4f) | 0x40);

        // Set variant to RFC 4122.
        bytes[8] = (byte) ((bytes[8] & 0xbf) | 0x80);

        return new Uuid(bytes, ByteOrder.BigEndian);
    }

}
